import os
import socket
import re
from collections import namedtuple, defaultdict
from datetime import date, datetime, timedelta
import trio
from sqlalchemy import Table, Column, Integer, String, DateTime, ForeignKey
import sqlalchemy
from sqlalchemy_aio import TRIO_STRATEGY
import logging
import time


# Config
flush_time = int(os.environ.get("DNSLOG_FLUSH_TIME", 6))
listen_port = int(os.environ.get("DNSLOG_LISTEN_PORT", 9999))
sql_conn_string = os.environ.get("DNSLOG_DB_CONNECTION_STRING", "postgresql+psycopg2://postgres:PqRYMV4ygHKVFVEJJPS73juj@localhost:5432/dns")
log_mode = os.environ.get("DNSLOG_LOG_MODE", 'ERROR')
# Turn the logged query types into set of bytes
query_types = os.environ.get("DNSLOG_QUERY_TYPES", 'A|AAAA').split('|')


metadata = sqlalchemy.MetaData()
Hosts = sqlalchemy.Table(
    'hosts', metadata,
    Column('id', Integer, primary_key=True),
    Column('host', String(100)),
    Column('ip', String(100)),
)
Requests = sqlalchemy.Table(
    'requests', metadata,
    Column('id', Integer, primary_key=True),
    Column('host', None, ForeignKey("hosts.id")),
    Column('datetime', DateTime),
    Column('address', String(100)),
    Column('count', Integer)
)

engine = sqlalchemy.create_engine(sql_conn_string, strategy=TRIO_STRATEGY)

Query = namedtuple('Query', 'type address host')
Host = namedtuple('Host', 'hostname ip')
Domain = namedtuple('Domain', 'host address date')
def get_host(ip):
    '''Get the friendly hostname for a device'''
    try:
        hostname = socket.gethostbyaddr(ip)[0]
        return Host(hostname, ip)
    except socket.error:
        return Host(ip, ip)

logging.getLogger().setLevel(log_mode)

hosts = {}
requests = defaultdict(int)
def reset_mem_dbs():
    global hosts
    global requests
    # In-memory DB's
    hosts = {}
    requests = defaultdict(int)


async def socket_loop():
    # Create the listening socket
    listen_socket = trio.socket.socket(trio.socket.AF_INET6, trio.socket.SOCK_DGRAM)
    await listen_socket.bind(('::', listen_port))

    while True:
        data = await listen_socket.recv(1500)
        # Check if it matches a DNS Request
        if m := re.match(rb'.*dnsmasq\[\d+\]: query\[(?P<type>\w+)\] (?P<address>[\w\d\.\-]+) from (?P<host>[\w\d\.\-:]+)', data):
            # Make a query namedtuple for easy use, convert vals to str
            q = Query(**{
                key: val.decode() for key, val in m.groupdict().items()
            })  
            if q.type not in query_types:
                continue
            # Try to find the friendly hostname
            try:
                h = hosts[q.host]
            except KeyError:
                h = get_host(q.host)
                hosts[q.host] = h
            # Add to in-memory DB
            r = Domain(h, q.address, date.today())
            requests[r] += 1
            count = requests[r] 


async def db_flush_loop():
    while True:
        await trio.sleep(flush_time)

        # Get own copy of requests to work with
        global requests
        reqs = requests
        reset_mem_dbs()
        print(f"flushing {len(reqs)} to db")

        # Connect to the DB
        conn = await engine.connect()
        # Loop through the current reqs
        for domain, count in reqs.items():
            try:
                dt, host, ip, address = (domain.date, domain.host.hostname,
                                        domain.host.ip, domain.address)
                
                
                host_query = await conn.execute(
                    Hosts.select(sqlalchemy.sql.and_(
                        Hosts.c.host == host,
                        Hosts.c.ip == ip,
                    ))
                )
                host_row = await host_query.first()
                if not host_row:
                    ins = await conn.execute(
                        Hosts.insert().values(
                            host=host,
                            ip=ip,
                        )
                    )
                    host_row_id = ins.inserted_primary_key[0]
                else:
                    host_row_id = host_row['id']
                
                request_query = await conn.execute(
                    Requests.select(sqlalchemy.sql.and_(
                        Requests.c.host == host_row_id,
                        Requests.c.datetime == dt,
                        Requests.c.address == address
                    )
                ))
                req_row = await request_query.first()
                if not req_row:
                    ins = await conn.execute(Requests.insert().values(
                        host=host_row_id,
                        datetime=dt,
                        address=address,
                        count=count
                    ))
                else:
                    upd = await conn.execute(Requests.update().\
                        where(Requests.c.id == req_row.id).\
                        values(
                            count=req_row.count+count
                        )
                    )
            except sqlalchemy.exc.OperationalError:
                logging.error(f"DB error writing row {host} {address}")
        await conn.close()  # Close off our connection at the end 

async def main():
    metadata.create_all(engine.sync_engine)
    async with trio.open_nursery() as nursery:
        nursery.start_soon(socket_loop)
        nursery.start_soon(db_flush_loop)

if __name__ == "__main__":
    previous_exception = datetime.now()
    allowed_time_between_exc = timedelta(seconds=120)
    while True:
        try:
            trio.run(main)
        except KeyboardInterrupt as e:
            raise e
        except Exception as e:
            logging.exception(e)
            if datetime.now() - previous_exception < allowed_time_between_exc:
                # We are crashing too fast
                exit(1)
            
            logging.error(f"reseting in-mem DB's and sleeping for 60s")
            reset_mem_dbs()
            previous_exception = datetime.now()
            time.sleep(60)

